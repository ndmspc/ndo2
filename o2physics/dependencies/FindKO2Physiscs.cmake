# Copyright 2019-2020 CERN and copyright holders of ALICE O2.
# See https://alice-o2.web.cern.ch/copyright for details of the copyright holders.
# All rights not expressly granted are reserved.
#
# This software is distributed under the terms of the GNU General Public
# License v3 (GPL Version 3), copied verbatim in the file "COPYING".
#
# In applying this license CERN does not waive the privileges and immunities
# granted to it by virtue of its status as an Intergovernmental Organization
# or submit itself to any jurisdiction.

# add KFParticle::KFParticle as library to targets depending on KFParticle


#if (NOT DEFINED KFParticle_DIR)
#        set(KFParticle_DIR "$ENV{O2PHYSICS_ROOT}")
#endif()

find_path(O2PHYSICS_INCLUDE_DIR Core/TrackSelection.h
        PATH_SUFFIXES "include"
        HINTS "$ENV{O2PHYSICS_ROOT}")
find_library(O2PHYSICS_LIBPATH "AnalysisCore"
        PATH_SUFFIXES "lib"
        HINTS "$ENV{O2PHYSICS_ROOT}")

mark_as_advanced(O2PHYSICS_LIBPATH)

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(O2Physics DEFAULT_MSG
                                  O2PHYSICS_LIBPATH O2PHYSICS_INCLUDE_DIR)

if(O2PHYSICS_FOUND)
   set(O2PHYSICS_LIBRARIES ${O2PHYSICS_LIBPATH})
   set(O2PHYSICS_INCLUDE_DIRS ${O2PHYSICS_INCLUDE_DIR})
   # add target
#    if(NOT TARGET KFParticle::KFParticle)
#       add_library(KFParticle::KFParticle IMPORTED INTERFACE)
#       set_target_properties(KFParticle::KFParticle PROPERTIES
#         INTERFACE_LINK_LIBRARIES "${O2PHYSICS_LIBRARIES}"
#         INTERFACE_INCLUDE_DIRECTORIES "${O2PHYSICS_INCLUDE_DIR}")
#    endif()
endif()
