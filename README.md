# ndo2

## Installation via aliBuild

### Download `ndo2` recipe

Following command will dowload `ndo2` recipe and save it in to `$HOME/alice/alidist/ndo2.sh`

```bash
curl https://gitlab.com/ndmspc/ndo2/-/raw/main/ci/ndo2.sh -o $HOME/alice/alidist/ndo2.sh
```

### Build

```bash
aliBuild build ndo2 --defaults o2 --debug
```

### Enter

```bash
alienv enter ndo2/latest-main-o2C
```

### Run test

```bash
o2-ndo2-ana-tracks --aod-file root://eos.ndmspc.io:1094//eos/ndmspc/scratch//o2tut/AO2D-LHC21k6-1.root --batch
```

## Compilation via cvmfs

### Configure alias

Add `alias` in `$HOME/.bashrc`. Note that one have to specify `O2Physiscs` version

```bash
alias ali='export WORK_DIR=/cvmfs/alice.cern.ch; export ALIBUILD_ARCH_PREFIX=el9-x86_64/Packages; source $WORK_DIR/$ALIBUILD_ARCH_PREFIX/O2Physics/daily-20250218-0000-1/etc/profile.d/init.sh'
```

### Source evnironment from cvmfs

```bash
ali
```

### Configure project

```bash
cd <ndo2-project>
mkdir build
cd build
cmake ../ -G Ninja -DCMAKE_INSTALL_PREFIX=/tmp/ndo2
```

### Build project

```bash
cmake --build . -- install
```

